package com.hamon.gapsiprojectjhmm.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hamon.gapsiprojectjhmm.db.ObjectBox
import com.hamon.gapsiprojectjhmm.di.controller.SearchControllerImpl
import com.hamon.gapsiprojectjhmm.model.QueryObject
import com.hamon.gapsiprojectjhmm.model.QuerySearch
import io.objectbox.Box
import io.objectbox.kotlin.boxFor
import kotlinx.coroutines.launch

class MainViewModel(private val searchController: SearchControllerImpl) : ViewModel() {

    private val queryBox: Box<QuerySearch> = ObjectBox.boxStore.boxFor()

    private val _itemList: MutableLiveData<MutableList<QueryObject.Item>> by lazy {
        MutableLiveData<MutableList<QueryObject.Item>>().apply { value = mutableListOf() }
    }
    val itemList: LiveData<MutableList<QueryObject.Item>> get() = _itemList

    private val _queryList: MutableLiveData<MutableList<QuerySearch>> by lazy {
        MutableLiveData<MutableList<QuerySearch>>().apply { value = mutableListOf() }
    }
    val queryList: LiveData<MutableList<QuerySearch>> get() = _queryList

    fun searchQuery(querySearch: String) {
        saveQuery(querySearch)
        viewModelScope.launch {
            _itemList.postValue(searchController.search(query = querySearch))
        }
    }

    private fun saveQuery(query: String) {
        queryBox.put(QuerySearch(query = query))
    }

    fun getSaveQueries() {
        _queryList.postValue(queryBox.all)
    }


}