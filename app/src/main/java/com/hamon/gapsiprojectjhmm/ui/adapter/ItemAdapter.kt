package com.hamon.gapsiprojectjhmm.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.hamon.gapsiprojectjhmm.R
import com.hamon.gapsiprojectjhmm.databinding.ItemCardBinding
import com.hamon.gapsiprojectjhmm.model.QueryObject

class ItemAdapter(private val itemList: MutableList<QueryObject.Item> = mutableListOf()) :
    RecyclerView.Adapter<ItemAdapter.ViewHolder>() {

    fun submitList(newItemList: MutableList<QueryObject.Item>) {
        itemList.clear()
        itemList.addAll(newItemList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            ItemCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(itemList[position])
    }

    override fun getItemCount(): Int = itemList.size

    inner class ViewHolder(private val binding: ItemCardBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: QueryObject.Item) {
            binding.apply {
                itemTitle.text = item.title
                itemPrice.text = root.resources.getString(R.string.price_item, item.price.toString())
                itemImage.load(item.image)
            }
        }

    }

}