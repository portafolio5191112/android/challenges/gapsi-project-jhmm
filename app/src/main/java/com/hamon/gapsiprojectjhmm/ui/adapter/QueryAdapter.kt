package com.hamon.gapsiprojectjhmm.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hamon.gapsiprojectjhmm.databinding.ItemQueryBinding
import com.hamon.gapsiprojectjhmm.model.QuerySearch

class QueryAdapter(private val queryList: MutableList<QuerySearch> = mutableListOf()) :
    RecyclerView.Adapter<QueryAdapter.ViewHolder>() {

    fun submitList(newQueryList: MutableList<QuerySearch>) {
        queryList.clear()
        queryList.addAll(newQueryList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(ItemQueryBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(queryList[position])
    }

    override fun getItemCount(): Int = queryList.size

    inner class ViewHolder(private val binding: ItemQueryBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(query: QuerySearch) {
            binding.apply {
                queryString.text = query.query
            }
        }

    }

}