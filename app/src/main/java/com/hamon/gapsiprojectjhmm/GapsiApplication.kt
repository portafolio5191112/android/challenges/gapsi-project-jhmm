package com.hamon.gapsiprojectjhmm

import android.app.Application
import com.hamon.gapsiprojectjhmm.db.ObjectBox
import com.hamon.gapsiprojectjhmm.di.modules.Modules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class GapsiApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@GapsiApplication)
            androidFileProperties()
            koin.loadModules(
                arrayListOf(
                    Modules.api,
                    Modules.viewModel
                )
            )
            koin.createRootScope()
        }
        ObjectBox.init(this@GapsiApplication)
    }

}