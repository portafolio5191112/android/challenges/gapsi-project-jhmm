package com.hamon.gapsiprojectjhmm.provider.services

import com.hamon.gapsiprojectjhmm.model.QueryObject
import com.hamon.gapsiprojectjhmm.provider.utils.SEARCH
import retrofit2.http.*

interface SearchServices {

    @Headers("X-IBM-Client-Id:adb8204d-d574-4394-8c1a-53226a40876e")
    @GET(SEARCH)
    suspend fun search(@QueryMap query: Map<String, String>): QueryObject.Response

}