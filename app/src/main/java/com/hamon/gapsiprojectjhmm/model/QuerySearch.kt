package com.hamon.gapsiprojectjhmm.model

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id

@Entity
data class QuerySearch(
    @Id var id: Long = 0,
    var query: String = ""
)