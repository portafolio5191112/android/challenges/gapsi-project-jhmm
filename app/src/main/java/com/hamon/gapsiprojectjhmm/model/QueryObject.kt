package com.hamon.gapsiprojectjhmm.model

import com.google.gson.annotations.SerializedName

object QueryObject {

    data class Response(
        @SerializedName("totalResults") val totalResults: Int = 0,
        @SerializedName("page") val page: Int = 0,
        @SerializedName("items") val items: MutableList<Item> = mutableListOf(),
    )

    data class Item(
        @SerializedName("id") val id: String = "",
        @SerializedName("rating") val rating: Double = 0.0,
        @SerializedName("price") val price: Double = 0.0,
        @SerializedName("image") val image: String = "",
        @SerializedName("title") val title: String = "",
    )

}