package com.hamon.gapsiprojectjhmm.di.controller

import com.hamon.gapsiprojectjhmm.model.QueryObject
import com.hamon.gapsiprojectjhmm.provider.API
import com.hamon.gapsiprojectjhmm.provider.services.SearchServices

class SearchControllerImpl : SearchController {

    private val api = API.getServices<SearchServices>()

    override suspend fun search(query: String): MutableList<QueryObject.Item> {
        return try {
            val response = api.search(mapOf(Pair("query", query)))
            response.items
        } catch (exception: Exception) {
            exception.printStackTrace()
            mutableListOf<QueryObject.Item>()
        }
    }

}

interface SearchController {

    suspend fun search(query: String): MutableList<QueryObject.Item>

}