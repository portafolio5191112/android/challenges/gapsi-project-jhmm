package com.hamon.gapsiprojectjhmm.di.modules

import com.hamon.gapsiprojectjhmm.di.controller.SearchControllerImpl
import com.hamon.gapsiprojectjhmm.viewmodel.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


object Modules{

    val api = module {
        single { SearchControllerImpl() }
    }

    val viewModel = module {
        viewModel { MainViewModel(get()) }
    }
}
