package com.hamon.gapsiprojectjhmm.db

import android.content.Context
import com.hamon.gapsiprojectjhmm.model.MyObjectBox
import io.objectbox.BoxStore

object ObjectBox {

    lateinit var boxStore: BoxStore
        private set

    fun init(context: Context) {
        boxStore = MyObjectBox.builder().androidContext(context.applicationContext).build()
    }

}